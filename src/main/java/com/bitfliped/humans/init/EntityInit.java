package com.bitfliped.humans.init;

import com.bitfliped.humans.entity.EntityHuman;
import com.bitfliped.humans.util.Constants;
import com.bitfliped.humans.Humans;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.common.registry.EntityRegistry;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class EntityInit {
    public static Set<Biome> humanBiomes = new HashSet<>();
    public static void registerEntities() {
        registerEntity("human", EntityHuman.class, Constants.ENTITY_HUMAN, 59, 0xFFFFFF, 0x000000);
    }

    public static void registerEntitySpawns() {
        humanBiomes.addAll(BiomeDictionary.getBiomes(BiomeDictionary.Type.PLAINS));
        humanBiomes.addAll(BiomeDictionary.getBiomes(BiomeDictionary.Type.JUNGLE));
        humanBiomes.addAll(BiomeDictionary.getBiomes(BiomeDictionary.Type.FOREST));
        humanBiomes.addAll(BiomeDictionary.getBiomes(BiomeDictionary.Type.HILLS));
        EntityRegistry.addSpawn(EntityHuman.class, 10, 1, 1, EnumCreatureType.CREATURE, humanBiomes.toArray(new Biome[0]));
    }

    private static void registerEntity(String name, Class<? extends Entity> entityIn, int id, int range, int color1, int color2) {
        EntityRegistry.registerModEntity(new ResourceLocation(Constants.MODID + ":" + name), entityIn, name, id, Humans.INSTANCE, range, 1, true, color1, color2);
    }

}
