package com.bitfliped.humans.handlers;

import com.bitfliped.humans.init.EntityInit;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber
public class RegistryHandler {

    @SubscribeEvent
    public static void onModelRegister(ModelRegistryEvent e) {

    }

    public static void preInitRegistries() {
        EntityInit.registerEntities();
    }

    public static void initRegistries() {
        EntityInit.registerEntitySpawns();
    }

    public static void postInitRegistries() {

    }

}
