package com.bitfliped.humans;

import com.bitfliped.humans.handlers.RegistryHandler;
import com.bitfliped.humans.helpers.ConfigHelper;
import com.bitfliped.humans.proxy.CommonProxy;
import com.bitfliped.humans.util.Constants;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

import java.io.File;

@Mod(modid = Constants.MODID, name = Constants.NAME, version = Constants.VERSION)
public class Humans {
    @Mod.Instance
    public static Humans INSTANCE;

    @SidedProxy(clientSide = Constants.CLIENT, serverSide = Constants.SERVER)
    public static CommonProxy proxy;

    public static ConfigHelper.HumansConfig CONFIG;

    public static Logger logger;

    @Mod.EventHandler
    public static void preInit(FMLPreInitializationEvent e) {
        File configFolder = new File(e.getModConfigurationDirectory().getAbsolutePath() + "/Humans/");
        if (!configFolder.exists()) configFolder.mkdirs();
        CONFIG = ConfigHelper.getConfig(configFolder.getAbsolutePath() + "/config.json");
        logger = e.getModLog();
        proxy.handlePreInitEvent(e);
    }

    @Mod.EventHandler
    public static void init(FMLInitializationEvent e) {
        proxy.handleInitEvent(e);
    }

    @Mod.EventHandler
    public static void postInit(FMLPostInitializationEvent e) { }

}
