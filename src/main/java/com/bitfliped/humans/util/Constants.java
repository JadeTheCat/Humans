package com.bitfliped.humans.util;

public class Constants {
    public static final String NAME = "Humans";
    public static final String MODID = "humans";
    public static final String VERSION = "0.1.2";

    public static final String CLIENT = "com.bitfliped.humans.proxy.ClientProxy";
    public static final String SERVER = "com.bitfliped.humans.proxy.CommonProxy";

    public static final int ENTITY_HUMAN = 42;
}
