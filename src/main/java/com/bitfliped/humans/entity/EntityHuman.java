package com.bitfliped.humans.entity;

import com.bitfliped.humans.entity.ai.EntityAIFindTree;
import com.bitfliped.humans.entity.ai.EntityAIHumanFollow;
import com.bitfliped.humans.entity.ai.EntityAIHumanWander;
import com.bitfliped.humans.handlers.SoundsHandler;
import com.bitfliped.humans.Humans;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.*;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.ISpecialArmor;

import javax.annotation.Nullable;
import java.util.Random;

//TODO: Refactor and add comments to my code
public class EntityHuman extends EntityTameable {

    private static final DataParameter<Integer> HUMAN_VARIANT = EntityDataManager.<Integer>createKey(EntityHuman.class, DataSerializers.VARINT);
    private static final DataParameter<Boolean> HUMAN_IS_LEGACY = EntityDataManager.<Boolean>createKey(EntityHuman.class, DataSerializers.BOOLEAN);
    private static final DataParameter<Boolean> HUMAN_WANDERING_PERSIST = EntityDataManager.createKey(EntityHuman.class, DataSerializers.BOOLEAN);
    private static final DataParameter<Integer> HUMAN_CURRENT_MODE = EntityDataManager.createKey(EntityHuman.class, DataSerializers.VARINT);

    protected EntityAIHumanWander humanWander;
    protected EntityAIHumanFollow humanFollow;
    protected EntityAIFindTree aiFindTree = new EntityAIFindTree(this, 15.0D);
    protected int mode;



    public EntityHuman(World worldIn) {
        super(worldIn);
        this.setSize(0.6F, 1.6F);
        this.setTamed(false);
    }

    @Nullable
    @Override
    public EntityAgeable createChild(EntityAgeable ageable) {
        return null;
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        this.dataManager.register(HUMAN_VARIANT, 0);
        this.dataManager.register(HUMAN_IS_LEGACY, false);
        this.dataManager.register(HUMAN_WANDERING_PERSIST, true);
        this.dataManager.register(HUMAN_CURRENT_MODE, 0);
    }

    @Override
    @Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata) {
        livingdata = super.onInitialSpawn(difficulty, livingdata);
        int v;
        boolean isLegacy;
        boolean isWandering;
        int mode;
        int r = this.rand.nextInt(20);
        if (livingdata instanceof EntityHuman.GroupData) {
            v = ((GroupData)livingdata).variant;
            isLegacy = (((GroupData) livingdata).isLegacy);
            isWandering = (((GroupData) livingdata).isWandering);
            mode = (((GroupData) livingdata).mode);
        } else {
            v = this.rand.nextInt(5);
            isLegacy = (v == 4 || r == 5);
            isWandering = true;
            mode = 0;
            livingdata = new GroupData(v, isLegacy, isWandering, mode);
        }

        this.setHumanVariant(v);
        this.setHumanIsLegacy(isLegacy);
        switch (this.getHumanVariant()) {
            case 0:
                this.setCustomNameTag("Steve");
                break;
            case 1:
                this.setCustomNameTag("Alex");
                break;
            case 2:
                this.setCustomNameTag("Steve");
                break;
            case 3:
                this.setCustomNameTag(getRandomCachedSkinUsername(this.rand));
                break;
            case 4:
                this.setCustomNameTag("Steve");
            default:
                break;
        }


        this.setSize(0.6F, 1.6F);
        return livingdata;
    }



    public static class GroupData implements IEntityLivingData {
        public int variant;
        public boolean isLegacy;
        public boolean isWandering;
        public int mode;

        public GroupData(int variantIn, boolean isLegacyIn, boolean isWanderingIn, int modeIn) {
            this.variant = variantIn;
            this.isLegacy = isLegacyIn;
            this.isWandering = isWanderingIn;
            this.mode = modeIn;
        }
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);
        this.setHumanVariant(compound.getInteger("Variant"));
        this.setHumanIsLegacy(compound.getBoolean("IsLegacy"));
        this.setHumanWanderingPersist(compound.getBoolean("WanderPersist"));
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound compound) {
        super.writeEntityToNBT(compound);
        compound.setInteger("Variant", this.getHumanVariant());
        compound.setBoolean("IsLegacy", this.getHumanIsLegacy());
        compound.setBoolean("WanderPersist", this.getHumanWanderingPersist());
    }

    public void setHumanVariant(int variant) {
        this.dataManager.set(HUMAN_VARIANT, Integer.valueOf(variant));
    }

    public int getHumanVariant() {
        return ((Integer)this.dataManager.get(HUMAN_VARIANT).intValue());
    }

    public void setHumanIsLegacy(boolean isLegacy) {
        this.dataManager.set(HUMAN_IS_LEGACY, isLegacy);
    }

    public boolean getHumanIsLegacy() {
        return ((Boolean)this.dataManager.get(HUMAN_IS_LEGACY).booleanValue());
    }

    public void setHumanWanderingPersist(boolean isWandering) { this.dataManager.set(HUMAN_WANDERING_PERSIST, isWandering); }

    public boolean getHumanWanderingPersist() { return ((Boolean) this.dataManager.get(HUMAN_WANDERING_PERSIST).booleanValue()); }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(2.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.10000000149011612D);
    }

    @Override
    protected void initEntityAI() {
        this.aiSit = new EntityAISit(this);
        this.humanWander = new EntityAIHumanWander(this, 2.0D);
        this.aiFindTree = new EntityAIFindTree(this, 15.0D);
        /*this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(2, this.aiSit);
        this.tasks.addTask(5, new EntityAIAttackMelee(this, 3.0D, true));
        this.tasks.addTask(6, new EntityAIFollowOwner(this, 3.0D, 10.0F, 2.0F));
        this.tasks.addTask(7, this.humanWander);
        this.tasks.addTask(8, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        this.tasks.addTask(8, new EntityAILookIdle(this));

        this.targetTasks.addTask(1, new EntityAIOwnerHurtByTarget(this));
        this.targetTasks.addTask(2, new EntityAIOwnerHurtTarget(this));*/

    }
    @Override
    public boolean isChild() {
        return false;
    }

    @Override
    protected SoundEvent getDeathSound() {
        if (this.getHumanIsLegacy() || this.getHumanVariant() == 4) {
            return SoundsHandler.ENTITY_HUMAN_HURT;
        } else {
            return super.getDeathSound();
        }
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        if (this.getHumanIsLegacy() || this.getHumanVariant() == 4) {
            return SoundsHandler.ENTITY_HUMAN_HURT;
        } else {
            return super.getHurtSound(damageSourceIn);
        }
    }
    private static String getRandomCachedSkinUsername(Random random) {
        int skinId = random.nextInt(Humans.proxy.getCacheSize());
        String username = Humans.proxy.getCacheUsernames()[skinId];
        return username;
    }

    @Override
    protected void damageEntity(DamageSource damageSrc, float damageAmount) {
        if (!this.isEntityInvulnerable(damageSrc)) {
            damageAmount = ForgeHooks.onLivingDamage(this, damageSrc, damageAmount);
            if (damageAmount <= 0) return;
            NonNullList<ItemStack> inventory = NonNullList.create();
            for (ItemStack stack : this.getArmorInventoryList()) {
                inventory.add(stack);
            }
            damageAmount = ISpecialArmor.ArmorProperties.applyArmor(this, inventory, damageSrc, damageAmount);
            if (damageAmount <= 0) return;
            damageAmount = this.applyPotionDamageCalculations(damageSrc, damageAmount);
            float f = damageAmount;
            damageAmount = Math.max(damageAmount - this.getAbsorptionAmount(), 0.0F);
            this.setAbsorptionAmount(this.getAbsorptionAmount() - (f - damageAmount));
            damageAmount = ForgeHooks.onLivingDamage(this, damageSrc, damageAmount);
            if (damageAmount != 0.0F)
            {
                float f1 = this.getHealth();
                this.setHealth(this.getHealth() - damageAmount);
                this.getCombatTracker().trackDamage(damageSrc, f1, damageAmount);
            }
            if (damageSrc.getTrueSource() instanceof EntityLivingBase) {
                if (shouldAttackEntity((EntityLivingBase) damageSrc.getTrueSource(), this)) {
                    this.setAttackTarget((EntityLivingBase) damageSrc.getTrueSource());
                }
            } else if (damageSrc.getTrueSource() instanceof EntityArrow) {
                Entity projectilesrc = ((EntityArrow) damageSrc.getTrueSource()).shootingEntity;
                if (projectilesrc instanceof EntityLivingBase) {
                    if (shouldAttackEntity((EntityLivingBase) projectilesrc, this)) {
                        this.setAttackTarget((EntityLivingBase) projectilesrc);
                    }
                }
            }
        }
    }

    @Override
    protected void damageArmor(float damage) {
        NonNullList<ItemStack> inventory = NonNullList.create();
        for (ItemStack stack : this.getArmorInventoryList()) {
            stack.damageItem(Math.round(damage), this);
        }

    }

    @Override
    public void swingArm(EnumHand hand) {
        super.swingArm(hand);
    }



    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand) {
        /*if (!this.world.isRemote) {
            ItemStack stack = player.getHeldItem(hand);
            Item item = stack.getItem();
            if (!this.isTamed()) {
                if (stack.getItem() == Items.DIAMOND) {
                    player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                    this.setTamedBy(player);
                    if (!player.isCreative()) {
                        stack.shrink(1);
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                if (stack.isEmpty()) {
                    if (this.isOwner(player) && !this.world.isRemote) {
                        //this.aiSit.setSitting(!this.isSitting());
                        this.humanWander.setWandering(!this.getHumanWanderingPersist());
                        player.sendMessage(new TextComponentString(Boolean.toString(this.humanWander.wandering)));
                        this.isJumping = false;
                        this.navigator.clearPath();
                        this.setAttackTarget((EntityLivingBase) null);
                        player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, 10.0F, 1.0F);
                        return true;
                    } else {
                        return false;
                    }
                } else if (item instanceof ItemSword) {
                    this.setHeldItem(EnumHand.MAIN_HAND, stack.copy());
                    if (!player.isCreative()) {
                        stack.shrink(1);
                    }
                    return true;
                } else if (item instanceof ItemArmor) {
                    ItemArmor armor = (ItemArmor) item;
                    this.setItemStackToSlot(armor.getEquipmentSlot(), stack.copy());
                    if (!player.isCreative()) {
                        stack.shrink(1);
                    }
                    return true;
                }
                return false;
            }
        } else {
            return true;
        }*/

        if (!this.world.isRemote) {
            BlockPos pos = this.aiFindTree.findTree();
            return true;
        } else {
            return true;
        }
    }

    @Override
    public void onLivingUpdate() {
        if (this.getHealth() < this.getMaxHealth() && this.ticksExisted % 100 == 0) {
            this.heal(1.0F);
        }
        super.onLivingUpdate();
    }

    @Override
    public boolean shouldAttackEntity(EntityLivingBase target, EntityLivingBase owner) {
        if (target instanceof EntityPlayer && owner instanceof EntityPlayer && !((EntityPlayer)owner).canAttackPlayer((EntityPlayer)target)) {
            return false;
        } else {
            return !(target instanceof AbstractHorse) || !((AbstractHorse)target).isTame();
        }
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        if (this.isEntityInvulnerable(source))
        {
            return false;
        }
        else
        {
            Entity entity = source.getTrueSource();

            if (this.aiSit != null)
            {
                this.aiSit.setSitting(false);
            }

            if (entity != null && !(entity instanceof EntityPlayer) && !(entity instanceof EntityArrow))
            {
                amount = (amount + 1.0F) / 2.0F;
            }



            return super.attackEntityFrom(source, amount);
        }
    }

    @Override
    public boolean attackEntityAsMob(Entity entityIn) {
        boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), (float)((int)this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue()));

        if (flag)
        {
            this.applyEnchantments(this, entityIn);
        }

        return flag;
    }
}
