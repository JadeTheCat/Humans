package com.bitfliped.humans.entity.ai;

import com.bitfliped.humans.entity.EntityHuman;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIWander;

public class EntityAIHumanWander extends EntityAIWander {
    private EntityHuman human;

    public boolean wandering;

    public EntityAIHumanWander(EntityHuman humanIn, double speedIn) {
        super(humanIn, speedIn);
        this.human = humanIn;
    }

    @Override
    public boolean shouldExecute() {
        if (!this.human.isTamed())
        {
            return false;
        }
        else if (!this.human.onGround) {
            return false;
        }
        else
        {
            EntityLivingBase entitylivingbase = this.human.getOwner();

            if (entitylivingbase == null)
            {
                return true;
            }
            else
            {
                return this.human.getDistanceSq(entitylivingbase) < 144.0D && entitylivingbase.getRevengeTarget() != null ? false : this.wandering;
            }
        }
    }

    @Override
    public void startExecuting() {
        this.human.getNavigator().clearPath();
        this.human.setHumanWanderingPersist(true);
    }

    @Override
    public void resetTask()
    {
        this.human.setHumanWanderingPersist(false);
    }

    public void setWandering(boolean wanderingIn) { this.wandering = wanderingIn; }
}
