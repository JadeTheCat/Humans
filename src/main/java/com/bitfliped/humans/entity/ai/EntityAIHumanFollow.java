package com.bitfliped.humans.entity.ai;

import com.bitfliped.humans.entity.EntityHuman;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIFollowOwner;
import net.minecraft.entity.passive.EntityTameable;

public class EntityAIHumanFollow extends EntityAIFollowOwner {

    private EntityHuman human;

    public boolean following;

    public EntityAIHumanFollow(EntityTameable tameableIn, double followSpeedIn, float minDistIn, float maxDistIn) {
        super(tameableIn, followSpeedIn, minDistIn, maxDistIn);
        this.human = (EntityHuman) tameableIn;
    }

    @Override
    public boolean shouldExecute() {
        if (!this.human.isTamed())
        {
            return false;
        }
        else if (!this.human.onGround) {
            return false;
        }
        else
        {
            EntityLivingBase entitylivingbase = this.human.getOwner();

            if (entitylivingbase == null)
            {
                return true;
            }
            else
            {
                return this.human.getDistanceSq(entitylivingbase) < 144.0D && entitylivingbase.getRevengeTarget() != null ? false : this.following;
            }
        }
    }

    @Override
    public void startExecuting() {
        this.human.getNavigator().clearPath();
        this.human.setHumanWanderingPersist(true);
    }

    @Override
    public void resetTask()
    {
        this.human.setHumanWanderingPersist(false);
    }

    public void setFollowing(boolean followingIn) { this.following = followingIn; }
}
