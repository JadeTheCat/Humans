package com.bitfliped.humans.entity.ai;

import com.bitfliped.humans.entity.EntityHuman;
import net.minecraft.block.BlockLog;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.lwjgl.Sys;

import java.util.ArrayList;

public class EntityAIFindTree extends EntityAIBase {

    EntityHuman human;

    double maxDistance;

    public EntityAIFindTree(EntityHuman humanIn, double maxDistanceIn) {
        this.human = humanIn;
        this.maxDistance = maxDistanceIn;
    }

    @Override
    public boolean shouldExecute() {
        return false;
    }

    public BlockPos findTree() {
        Vec3d lookVec = human.getLookVec();
        double angle = Math.toRadians(45.0D);
        int angleIncrements = 45;
        ArrayList<BlockPos> possiblePositions = new ArrayList<>();
        World world = this.human.world;
        Vec3d startVec = this.human.getPositionVector().add(new Vec3d(0D, this.human.getEyeHeight(), 0D));
        Vec3d humanLookVec = this.human.getLookVec();
        System.out.println(humanLookVec);
        for (int i = 0; i < angleIncrements; i++) {
            double endX = Math.sin(humanLookVec.x + Math.toRadians(i)) * 15;
            double endZ = Math.cos(humanLookVec.y + Math.toRadians(i)) * 15;
            double endY = 0.0D;



            Vec3d endVec = new Vec3d(endX, endY, endZ);

            RayTraceResult result = world.rayTraceBlocks(startVec, startVec.add(endVec));
            System.out.println(endVec);
            if (result != null && world.getBlockState(result.getBlockPos()).getBlock() instanceof BlockLog) {
                System.out.printf("%d, %d, %d%n", result.getBlockPos().getX(), result.getBlockPos().getY(), result.getBlockPos().getZ());
            } else {
                System.out.println("No hit");
            }
        }
        /*
        for (int i = 0; i > -angleIncrements; i--) {
            double endX = Math.sin(humanLookVec.x + Math.toRadians(i)) * 15;
            double endZ = Math.cos(humanLookVec.y + Math.toRadians(i)) * 15;
            double endY = 0.0D;



            Vec3d endVec = new Vec3d(endX, endY, endZ);

            RayTraceResult result = world.rayTraceBlocks(startVec, startVec.add(endVec));
            System.out.println(startVec.add(endVec));
            if (result != null && world.getBlockState(result.getBlockPos()).getBlock() instanceof BlockLog) {
                System.out.printf("%d, %d, %d%n", result.getBlockPos().getX(), result.getBlockPos().getY(), result.getBlockPos().getZ());
            } else {
                System.out.println("No hit");
            }
        }*/



        return new BlockPos(0, -10, 0);
    }
}
