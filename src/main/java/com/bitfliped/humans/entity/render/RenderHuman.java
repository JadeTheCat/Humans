package com.bitfliped.humans.entity.render;

import com.bitfliped.humans.Humans;
import com.bitfliped.humans.entity.EntityHuman;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerBipedArmor;
import net.minecraft.client.renderer.entity.layers.LayerHeldItem;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;

public class RenderHuman extends RenderLiving<EntityHuman> {

    protected int variant = 0;

    public static final ResourceLocation STEVE = new ResourceLocation("minecraft:textures/entity/steve.png");
    public static final ResourceLocation ALEX = new ResourceLocation("minecraft:textures/entity/alex.png");
    public static final ResourceLocation LEGASTEVE = new ResourceLocation("humans:textures/entity/legasteve.png");
    public static final ResourceLocation THINSTEVE = new ResourceLocation("humans:textures/entity/thinsteve.png");

    private static final ModelPlayer defaultModel = new ModelPlayer(0.0F, false);

    public ResourceLocation skin = STEVE;

    public RenderHuman(RenderManager manager) {
        super(manager, defaultModel, 0.5F);
        this.addLayer(new LayerHeldItem(this));
        this.addLayer(new LayerBipedArmor(this));
    }
    @Override
    protected void preRenderCallback(EntityHuman entity, float f) {
        this.variant = entity.getHumanVariant();
        switch (this.variant) {
            case 0:
                this.skin = STEVE;
                this.mainModel = new ModelPlayer(0.0F, false);
                break;
            case 1:
                this.skin = ALEX;
                this.mainModel = new ModelPlayer(0.0F, true);
                break;
            case 2:
                this.skin = THINSTEVE;
                this.mainModel = new ModelPlayer(0.0F, true);
                break;
            case 3:
                this.skin = Humans.proxy.getSkinFromCache(entity.getCustomNameTag());
                this.mainModel = new ModelPlayer(0.0F, Humans.proxy.getIsSlim(entity.getCustomNameTag()));
                break;
            case 4:
                this.skin = LEGASTEVE;
                this.mainModel = new ModelPlayer(0.0F, false);
                break;
            default:
                this.skin = STEVE;
                this.mainModel = new ModelPlayer(0.0F, false);
                break;
        }
        this.mainModel.isChild = entity.isChild();
        this.mainModel.isRiding = entity.isSitting();
        if (this.mainModel.isRiding) {
            GlStateManager.translate(0.0F, 0.5F, 0.0F);
        }
        GlStateManager.scale(0.94F, 0.94F, 0.9F);
    }

    @Override
    protected boolean canRenderName(EntityHuman entity) {
        return true;
    }

    @Override
    public void doRender(EntityHuman entity, double x, double y, double z, float entityYaw, float partialTicks) {
        ModelPlayer model = (ModelPlayer)this.getMainModel();
        ModelBiped.ArmPose armPose = ModelBiped.ArmPose.EMPTY;
        if (!entity.getHeldItem(EnumHand.MAIN_HAND).isEmpty()) {
            armPose = ModelBiped.ArmPose.ITEM;
            model.rightArmPose = armPose;
        }
        super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    @Nullable
    @Override
    protected ResourceLocation getEntityTexture(EntityHuman entity) {
        return skin;
    }





}
