package com.bitfliped.humans.proxy;

import com.bitfliped.humans.Humans;
import com.bitfliped.humans.handlers.RegistryHandler;
import com.bitfliped.humans.handlers.SoundsHandler;
import com.bitfliped.humans.helpers.ConfigHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;

import java.util.HashSet;
import java.util.Set;

public class CommonProxy {
    private Set<String> skinList = new HashSet<>();
    public ResourceLocation getSkinFromCache(String username) {
        return null;
    }
    public int getCacheSize() { return skinList.size(); }
    public String[] getCacheUsernames() { return skinList.toArray(new String[0]); }
    public boolean getIsSlim(String username) { return false; };
    public void handleInitEvent(FMLInitializationEvent e) {
        RegistryHandler.initRegistries();
        skinList.add("Steve");
        for (ConfigHelper.HumansSkin skin : Humans.CONFIG.skins) {
            skinList.add(skin.username);
        }
        SoundsHandler.registerSounds();
    }
    public void handlePreInitEvent(FMLPreInitializationEvent e) {
        RegistryHandler.preInitRegistries();
    }
    public void handlePostInitEvent(FMLPostInitializationEvent e) { RegistryHandler.postInitRegistries(); }
}
